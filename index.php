<?php 
    include "db_connection.php"; 
    include "requete.php"; 
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--BOOSTRAP-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href=".//asset/css/veille.css">
    <title>Veille</title>
</head>

<body>

    <!--Navbar-->

    <nav>
        <input type="checkbox" id="check">
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>
        <label class="logo"><img src="../asset/img/airtable.png" alt="logo" width="80px" height="80px"></label>
        <ul>
            <!-- <li><a href="veille.php">Veille</a></li> -->
            <li><button type="button" class="btn btn-primary"><a href="index.php">Veille</a></button></li>

        </ul>
    </nav>

    <main>
        <div class="container" style="color: white">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <?php
                        for ($i=0; $i < count($showVeille); $i++) { 
                            ?>
                                <div class="row">
                                    <div class="col-4">
                                        
                                        <div style="background: url(<?php echo $showVeille[$i]['image_url'] ?>) center / cover; height: 100px;"></div>
                                    </div>
                                    <div class="col-8">
                                        <h3><?php echo utf8_encode($showVeille[$i]['sujet']); ?></h3>
                                        <p><?php echo utf8_encode($showVeille[$i]['synthese']); ?></p>
                                        <p><?php echo utf8_encode($showVeille[$i]['commentaire']); ?></p>
                                        <p><?php echo $showVeille[$i]['date']; ?></p>
                                    </div>
                                    
                                </div>
                                <hr>
                            <?php
                        }
                    ?>
                </div>
                <div class="col-12 col-lg-4">

                </div>
            </div>
        </div>
    </main>
    <!--Contenus-->

    <div class="container">



        

</body>

</html>