<?php

    function debug($var, $die=false) {
        echo '<pre style="color: white;">';
        print_r($var);
        echo '</pre>';
        if($die) die;
    }

  $host = 'localhost';
  $dbname = 'my_veille';
  $username = 'root';
  $password = '';
  $error = false;
    
  $dsn = "mysql:host=$host;dbname=$dbname"; 
  // récupérer toutes les veilles
  $sql = "SELECT * FROM veilles";
   
  try{
   $pdo = new PDO($dsn, $username, $password);
   $stmt = $pdo->query($sql);

   
   if($stmt === false){
    die("Erreur");
   }
   
  }catch (PDOException $e){
    echo $e->getMessage();
  }
?>